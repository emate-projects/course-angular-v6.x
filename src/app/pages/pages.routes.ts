/**
 * Created by pako8 on 22-Oct-17.
 */

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { config } from '.';
import { AuthGuardService } from ".";

const pagesPath = config.pagesPath;

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        // checks if the user is logged in
        // canActivate: [AuthGuardService],
        children: [
            {
                path: config.exercise_01,
                loadChildren: './ex_01/ex_01.module#Ex01Module'
            },
            {
                path: config.exercise_02,
                loadChildren: './ex_02/ex_02.module#Ex02Module'
            },
        ],
    },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
