import { Observable } from 'rxjs/Rx';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import {Component, Injectable} from '@angular/core';
// import { NotificationComponent } from "../../shared/widget/notification/notification.component";

@Injectable()
export class NotificationService {

    config: MatDialogConfig = { disableClose: true };

    constructor(private dialog: MatDialog) { }

    public loading(): MatDialogRef <NotificationComponent>{

        let dialogRef: MatDialogRef <NotificationComponent>;

        dialogRef = this.dialog.open(NotificationComponent, this.config)
            .updateSize('330px', '110px')
            .updatePosition({ bottom: '20px', right: '20px' });

        return dialogRef;
    }
}

@Component({
    selector: 'loading-notify',
    template: `
        <div class="dialog-text"><b>Caricamento...</b></div>
        <mat-spinner [diameter]="60" [strokeWidth]="2" mode="indeterminate" [color]="primary"></mat-spinner>
        <style>
            .dialog-text {
                color: rgba(0,0,0,0.5);
                font-size: 18px;
                float: left;
                vertical-align: middle;
                line-height: 60px;
                margin-left: 10px;
            }
            mat-spinner {
                float: right;
                margin-right:30px;
            }
        </style>
    `,
})
export class NotificationComponent {

    constructor(public dialogRef: MatDialogRef<NotificationComponent>) {

    }
}
