export const environment = {

    // environment
    production: true,

    // region
    region: 'eu-central-1',

    // cognito user pool
    identityPoolId: 'eu-central-1:a84c58ac-6064-4de5-b27f-bb834f5b0bd5',
    userPoolId: 'eu-central-1_KmfNyGMuJ',
    clientId: '418lfcts6dfrfretlu2g1ptqdg',
    appTitle: 'ethica-gdpr',

    // api gateway
    apiGatewayUrl: 'https://uqxynjiv77.execute-api.eu-central-1.amazonaws.com/svil/',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-1',

    ddbTableName: 'LoginTrail',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: '',

    landingPage: '/pages/articles',

    firebase : {
        apiKey: "AIzaSyDrroFHkKKPC_8Ba22pda9N-W2PBNxYeB8",
        authDomain: "arteriaculturale-site.firebaseapp.com",
        databaseURL: "https://arteriaculturale-site.firebaseio.com",
        projectId: "arteriaculturale-site",
        storageBucket: "arteriaculturale-site.appspot.com",
        messagingSenderId: "631311305859"
    }
};
