/**
 * Created by pako8 on 22-Oct-17.
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from "@angular/material";
import {ConfirmDialogComponent} from "./confirm-dialog.component";

const UIModules = [
    MatButtonModule
];

const dialogContents = [
    ConfirmDialogComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...UIModules
    ],
    declarations: [dialogContents],
    exports: [ ...dialogContents ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    entryComponents: [ConfirmDialogComponent]
})
export class DialogModule { }
