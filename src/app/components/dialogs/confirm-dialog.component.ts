/**
 * Created by pako8 on 04-Apr-17.
 */
import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
  selector: 'confirm-dialog',
  template: `
        <h1 class="dialog-text"><b>{{ title }}</b></h1>
        <p class="dialog-text">{{ message }}</p>
        <button type="button" color="primary" mat-raised-button 
            (click)="dialogRef.close(true)">OK</button>
        <button type="button" mat-button *ngIf="!onlyAccept"
            (click)="dialogRef.close()">Cancel</button>
        <style>
            .dialog-text {
                color: rgba(0,0,0,0.5);
                font-size: 18px;
                margin-bottom: 20px;
            }
        </style>
    `,
})
export class ConfirmDialogComponent {

  public title: string;
  public message: string;
  public onlyAccept: boolean;

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) { 

  }
}
