import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ChangeDetectorRef} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';
import {CrudService} from '../../../services/api/crud.service';
import {DialogService} from "../../../services/dialog/dialog.service";
import {NotificationService} from "../../../services/notification/notifications.service";

export class DataTableOptions {

    dbTableName: string;
    fields: Array<DataTableField>;
}

export class DataTableField {

    public label:string;
    public data:string;

    constructor(private key:string, private value:string) {
        this.label = key;
        this.data = value;
    }
}

@Component({
    providers: [CrudService, DialogService, NotificationService],
    selector: 'arteria-datatable',
    templateUrl: './datatable.html',
    styleUrls:['./datatable.scss']
})
export class ArteriaDatatableComponent implements OnInit {

    @Input() dtOptions:DataTableOptions;
    @Output() rowSelected: EventEmitter<object> = new EventEmitter<object>();

    columnValues:Array<string> = [];
    columns: Array<DataTableField> = [];
    static emptyModel: object;

    public dialog: MatDialogRef<any>;

    exampleDatabase:ExampleDatabase; //= new ExampleDatabase(this._crudService, this.dtOptions.dbTableName);
    selection = new SelectionModel<string>(true, []);
    dataSource: ExampleDataSource | null;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;

    constructor(private _crudService: CrudService, private _dialogsService: DialogService, private changeDetector: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.columnFactory();

        this.exampleDatabase = new ExampleDatabase(this._crudService, this.dtOptions.dbTableName);
        
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort, this.columnValues);

        Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(() => {
                if (!this.dataSource) { return; }
                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    ngAfterViewInit(){
        this.changeDetector.detectChanges();
    }

    columnFactory() {
        this.dtOptions.fields.forEach((columns) => {
            this.columns.push(columns);
            this.columnValues.push(columns.data);
        });
    }

    editItem(row) {

        let model;
        if(row === null) {
            // sets to null all the target object fields
            for(let field in ArteriaDatatableComponent.emptyModel){
                ArteriaDatatableComponent.emptyModel[field] = null;
            }
            model = { data: ArteriaDatatableComponent.emptyModel, disabled: false };
        } else {
            let selRow = Object.assign({}, row);
            model = { data: selRow, disabled: true };
        }
        // emits the event
        this.rowSelected.emit(model);
    }
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleDatabase {

    /** Stream that emits whenever the data has been modified. */
    dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    constructor(private _crudService: CrudService, private tableName: string) {

        let columns = new Array<Object>();

        for(let i = 0; i<=100; i++){

            let col = {usr_lastname:'pippo'+i, usr_firstname:'Franco'+i, usr_age:'2'+i, usr_city:'roma'+i, usr_uuid:'xaaddd'+i};

            columns.push(col);
            
        }

        this.addUser(columns.slice());

    }

    get data(): any[] {
        return this.dataChange.value;
    }

    /** Adds a new user to the database. */
    addUser(sources:any[]) {

        ArteriaDatatableComponent.emptyModel = Object.assign({}, sources[0]);

        for (let source of sources) {
            const copiedData = this.data.slice();
            copiedData.push(source);
            this.dataChange.next(copiedData);
        }
    }
}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class ExampleDataSource extends DataSource<any> {

    _filterChange = new BehaviorSubject('');
    get filter(): string { return this._filterChange.value; }
    set filter(filter: string) { this._filterChange.next(filter); }

    filteredData: any[] = [];
    renderedData: any[] = [];

    constructor(private _exampleDatabase: ExampleDatabase,
                private _paginator: MatPaginator,
                private _sort: MatSort,
                private columns: Array<string>) {
        super();

        // Reset to the first page when the user changes the filter.
        this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        // Listen for any changes in the base data, sorting, filtering, or pagination
        const displayDataChanges = [
            this._exampleDatabase.dataChange,
            this._sort.sortChange,
            this._filterChange,
            this._paginator.page,
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            // Filter data
            this.filteredData = this._exampleDatabase.data.slice().filter((item: any) => {
                let searchStr = '';
                for (let column of this.columns) {
                    searchStr += (item[column]);
                }
                return searchStr.toLowerCase().indexOf(this.filter.toLowerCase()) !== -1;
            });

            // Sort filtered data
            const sortedData = this.sortData(this.filteredData.slice());

            // Grab the page's slice of the filtered sorted data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
            return this.renderedData;
        });
    }

    disconnect() {}

    /** Returns a sorted copy of the database data. */
    sortData(data: any[]): any[] {
        if (!this._sort.active || this._sort.direction === '') { return data; }

        return data.sort((a, b) => {
            let propertyA: number|string = a[this._sort.active];
            let propertyB: number|string = b[this._sort.active];
            let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
    }
}
