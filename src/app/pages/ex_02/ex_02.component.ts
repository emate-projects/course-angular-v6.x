import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs/Rx';
import { DataTableOptions, DataTableField, CrudService, NotificationService} from '..';
import 'rxjs/add/operator/map';

@Component({
    providers: [CrudService, NotificationService],
    selector: 'app-home',
    templateUrl: './ex_02.html',
    styleUrls:['./ex_02.scss']
})
export class Ex02Component {


    constructor() {

    }


    update(model: any) {

    }

    insert(model: any) {

    }
}
