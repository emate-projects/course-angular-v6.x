import {Component, Input} from '@angular/core';


@Component({
    selector: 'max-slider',
    templateUrl: './slider.html',
    styleUrls:['./slider.scss']
})
export class MaxSliderComponent {

    @Input() slider;
}
