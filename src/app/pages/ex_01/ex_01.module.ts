/**
 * Created by pako8 on 22-Jun-17.
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ex01Component } from './ex_01.component';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatDialogModule, MatGridListModule, MatIconModule, MatInputModule, MatPaginatorModule, MatSlideToggleModule, MatTableModule} from "@angular/material";
import { ArteriaDatatableModule, MaxSliderModule, MaxFormModule } from "..";
import { CrudService } from '../../services';


const routes = [
    { path: '', component: Ex01Component }
];

const UIModules = [
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSlideToggleModule,
    ArteriaDatatableModule,
    MaxSliderModule,
    MaxFormModule,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...UIModules,
        RouterModule.forChild(routes)
    ],
    providers: [CrudService],
    declarations: [Ex01Component],
    exports: [RouterModule],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class Ex01Module { }
