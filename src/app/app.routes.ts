import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {config} from './config';

const appPath = config.appPath;

const routes: Routes = [
    {path: '', redirectTo: 'pages/ex_01', pathMatch: 'full'},
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }
        ]
    },
    {path: '**', redirectTo: 'pages/ex_01', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);