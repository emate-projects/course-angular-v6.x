/**
 * Created by pako8 on 22-Jun-17.
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaxSliderComponent } from './slider.component';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatGridListModule, MatIconModule, MatInputModule, MatPaginatorModule, MatTableModule} from "@angular/material";

const UIModules = [
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...UIModules
    ],
    declarations: [MaxSliderComponent],
    exports: [ MaxSliderComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MaxSliderModule { }
