import { Observable } from 'rxjs/Rx';
import { ConfirmDialogComponent } from '../../components/dialogs/confirm-dialog.component';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';
// import { ChangePasswordDialog } from "./change-password/change-pass-dialog.component";
// import { DiagramDialog } from "./diagram/diagram-dialog.component";

@Injectable()
export class DialogService {

    constructor(private dialog: MatDialog) { }

    public confirm(title: string, message: string, onlyAccept: boolean): MatDialogRef <ConfirmDialogComponent>{

        let dialogRef: MatDialogRef<ConfirmDialogComponent>;

        dialogRef = this.dialog.open(ConfirmDialogComponent);

        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;
        dialogRef.componentInstance.onlyAccept = onlyAccept;

        return dialogRef;
    }

  // public changePassword(): MatDialogRef <ChangePasswordDialog>{
  //
  //     let dialogRef: MatDialogRef<ChangePasswordDialog>;
  //     dialogRef = this.dialog.open(ChangePasswordDialog);
  //     return dialogRef;
  // }

  // public diagramDetails(stage:any, type:string, item:string): MatDialogRef <DiagramDialog>{
  //
  //     let config: MatDialogConfig = { disableClose: true };
  //     let dialogRef: MatDialogRef<DiagramDialog> = null;
  //
  //     if(type==="stage"){
  //         dialogRef = this.dialog.open(DiagramDialog, config)
  //             .updateSize('500px', '95%')
  //             .updatePosition({top: '20px', right: '20px', bottom: '20px'});
  //     }
  //     dialogRef.componentInstance.type = type;
  //     dialogRef.componentInstance.stage = stage;
  //     dialogRef.componentInstance.item = item;
  //
  //     return dialogRef;
  // }
}
