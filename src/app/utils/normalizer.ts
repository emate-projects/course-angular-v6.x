export class Normalizer {

    /**
     * manage this response:
     *
     {
      "infouser": null,
      "output": "[Row[{\"username\": \"prova1@test.it\", \"date_creation\": \"2016-10-22\", \"date_registration\": null, \"org_code\": \"it0125maxwellgroup\", \"role_code\": \"UTENTE\", \"user_activate\": false}], Row[{\"username\": \"test2@yahoo.it\", \"date_creation\": \"2016-11-03\", \"date_registration\": null, \"org_code\": \"it0125maxwellgroup\", \"role_code\": \"null\", \"user_activate\": false}], Row[{\"username\": \"test6@yahoo.it\", \"date_creation\": \"2016-10-28\", \"date_registration\": null, \"org_code\": \"it0125maxwellgroup\", \"role_code\": \"UTENTE\", \"user_activate\": false}]]",
      "statoRisposta": "OK"
      }
     */
    manageJSONResponse(response:any):any[]{
        console.log("manageJSONResponse:",response);
        let json = response.json();
        let r = json.output || null;
        try{
            return this.extractJSONfromRows(r);
        }catch(ex){
            return [];
        }
    }

    extractJSONfromRows(rows:string):any[]{
        let re1:RegExp = new RegExp(/Row\[/, "g");
        if (rows.indexOf("Row[") >= 0){
            rows = rows.replace(re1,"").replace(new RegExp(/\}\]/, "g"),"}");
            if (rows[0] === "[" && rows[rows.length-1] !== "]") rows += "]";
        }
        console.log(rows);
        return JSON.parse(rows);
    }

    normalizeJSON(jsonList:any[], propList:string[]): any[]{

        Object.keys(propList).forEach((k)=>{
            jsonList.forEach(element => {
                if (!element[k])
                    element[k] = element[propList[k]];
            });
        });

        return jsonList;
    }
}
