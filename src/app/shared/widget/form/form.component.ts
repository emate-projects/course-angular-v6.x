import {AfterViewInit, Component, ContentChild, ContentChildren, Directive, Input, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup} from "@angular/forms";

import 'rxjs/add/operator/map';

import { CrudService } from '../../../services/api/crud.service';

@Component({
    providers: [CrudService],
    selector: 'max-form',
    templateUrl: './form.html',
    styleUrls:['./form.scss']
})
export class MaxFormComponent {



    constructor() {

    }
}
