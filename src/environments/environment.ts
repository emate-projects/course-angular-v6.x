// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {

  // environment
  production: false,


  firebase : {
    apiKey: "AIzaSyDBP8xCbuC21qhx_mG7AuDAKlFIvfsy0kU",
    authDomain: "angular-course-e42f8.firebaseapp.com",
    databaseURL: "https://angular-course-e42f8.firebaseio.com",
    projectId: "angular-course-e42f8",
    storageBucket: "angular-course-e42f8.appspot.com",
    messagingSenderId: "3518455023"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.