import { Injectable } from '@angular/core';

import { MainObject } from '../../models/main-object.model';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

export class QueryParamObject {
    field:string;
    operator:any;
    value:string;
}

@Injectable()
export class CrudService {

    constructor(private afs: AngularFirestore) {
        
    }

    // used to include IDs in documents inside collection;
    public objectBuilder (obj: Array<MainObject>): Array<MainObject> {

        let newArray: Array<MainObject> = new Array<MainObject>();

        obj.map(data => {
            const newObj = data.payload.doc.data();  
            newObj.id = data.payload.doc.id;
            newArray.push(newObj);
        });

        return newArray;
    }

    public getCollection(collectionName: string): Observable<any> {

        let collection: AngularFirestoreCollection<any> = this.afs.collection(collectionName); 
        const collection$: Observable<any> = collection.snapshotChanges();
        return collection$;
    }

    getCollectionWhere(collectionName: string, query: QueryParamObject) {
        const collection: AngularFirestoreCollection<any> = this.afs.collection(collectionName, ref => ref.where(query.field, query.operator, query.value));
        const collection$: Observable<any> = collection.snapshotChanges();
        return collection$;
    }

    public getDocument(collectionName, documentName): Observable<any> {
        const document: AngularFirestoreDocument<any> = this.afs.collection(collectionName).doc(documentName);
        const document$: Observable<any> = document.valueChanges();
        return document$;
    }

    public getSubCollection(collectionName, documentName, subCollectionName): Observable<any> {
        const subCollection: AngularFirestoreCollection<any> = this.afs.collection(collectionName).doc(documentName).collection(subCollectionName);
        const subCollection$: Observable<any> = subCollection.snapshotChanges();
        return subCollection$;
    }

    public updateDocument(model:any, collectionName: string, documentName: string) {
        const document: AngularFirestoreDocument<any> = this.afs.collection(collectionName).doc(documentName);
        document.update(model);
    }
}




@Injectable()
export class PriceService {

}