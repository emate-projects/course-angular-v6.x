/**
 * Created by pako8 on 04-Mar-17.
 */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import { Router } from '@angular/router';
import { config } from '../../config';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.html',
  styleUrls: ['./sidenav.scss']
})
export class SidenavComponent {

    @Input() status;
    @Input() paddingLeft;
    @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Input() navActions;
    lockedMenu = false;
    loggedUser: string;

    pages = config.pages;
    auth = config.auth;

    home = config.home;
    clienti = config.clienti;
    login = config.LOGIN;

    constructor(private router: Router) {

    }

    toggleMenu() {
        if(!this.lockedMenu) {
            this.status = !this.status;
            this.toggle.emit(this.status);
        }
    }

    lockMenu() {
        this.lockedMenu = !this.lockedMenu;
    }

    navigateTo(main:string , page:string) {
        this.router.navigate([main + '/' + page]);
    }
}
