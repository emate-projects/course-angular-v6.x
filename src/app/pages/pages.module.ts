/**
 * Created by pako8 on 15-Oct-17.
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from "@angular/common/http";
import { routing } from './pages.routes';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

// GLOBAL UI
import {MatListModule, MatIconModule, MatRippleModule, MatMenuModule} from '@angular/material';
import { PagesComponent } from './pages.component';
import { AuthGuardService, SidenavComponent, HeaderComponent} from ".";
import { FirebaseAuthService } from '../services'

// import { HeaderComponent } from '../shared/components/header/header.component';
// import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';

const layoutComponents = [
    PagesComponent,
    SidenavComponent,
    HeaderComponent
    // HeaderComponent
];

const UIModules = [
    MatListModule,
    MatIconModule,
    MatRippleModule,
    MatMenuModule
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        ...UIModules,
        RouterModule,
        routing
    ],
    declarations: [...layoutComponents],
    providers: [AuthGuardService, FirebaseAuthService],
    schemas:[ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PagesModule { }
