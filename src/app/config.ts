export const config = {

    // MODULES PATH
    appPath: 'app/',
    pagesPath: 'app/pages/',

    // MAIN ROUTES
    pages: 'pages',
    auth: 'auth',

    // PAGES ROUTES
    exercise_01: 'ex_01',
    exercise_02: 'ex_02',
    exercise_03: 'ex_03',
    exercise_04: 'ex_04',
    exercise_05: 'ex_05',
    exercise_06: 'ex_06',
    exercise_07: 'ex_07',
    exercise_08: 'ex_08',
    exercise_09: 'ex_09',
    exercise_10: 'ex_10',

    home: 'home',
    clienti: 'clienti',
    articles: 'articles',

    // AUTHENTICATION ROUTES
    LOGIN: 'login',
    REGISTER: 'register',
    CONFIRM: 'confirm',
    FORGOT1: 'forgot1',
    FORGOT2: 'forgot2',
    NEWPWD: 'newpwd',
    RESEND: 'resend'
};
