/**
 * Created by pako8 on 22-Jun-17.
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ex02Component } from './ex_02.component';
import { CommonModule } from '@angular/common';


const routes = [
    { path: '', component: Ex02Component }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [Ex02Component],
    exports: [RouterModule]
})
export class Ex02Module { }
