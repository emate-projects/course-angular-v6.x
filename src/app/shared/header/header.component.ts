/**
 * Created by pako8 on 22-Jun-17.
 */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import { FirebaseAuthService } from '../../services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.html',
    styleUrls: ['./header.scss']
})
export class HeaderComponent {

    @Input() status;
    @Input() paddingLeft;
    @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();
    loggedUser;

    constructor(private router: Router, private _firebaseAuthService: FirebaseAuthService) {

    }

    toggleMenu() {

        this.status = !this.status;
        this.toggle.emit(this.status);
    }

}
