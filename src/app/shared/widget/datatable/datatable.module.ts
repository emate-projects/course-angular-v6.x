/**
 * Created by pako8 on 22-Oct-17.
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ArteriaDatatableComponent } from './datatable.component';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatGridListModule, MatIconModule, MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule} from "@angular/material";
import {ConfirmDialogComponent} from "../../../components/dialogs/confirm-dialog.component";
import {NotificationComponent} from "../../../services/notification/notifications.service";

const UIModules = [
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatGridListModule,
    MatButtonModule,
    MatIconModule,
    MatSortModule,
    MatProgressSpinnerModule
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...UIModules
    ],
    declarations: [ArteriaDatatableComponent, NotificationComponent, ConfirmDialogComponent],
    exports: [ ArteriaDatatableComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    entryComponents: [NotificationComponent, ConfirmDialogComponent]
})
export class ArteriaDatatableModule { }
