import {Component, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-pages-component',
    templateUrl: './pages.html',
    styleUrls: ['./pages.scss']
})

export class PagesComponent {

    status = true;

    private close = '50px';
    private open = '200px';
    protected barPaddingLeft;
    protected sideNavWidth;

    constructor(protected router: Router) {
        this.sideNavWidth = this.close;
        this.barPaddingLeft = this.sideNavWidth;
    }

    toggle(event) {
        if (this.status) {
            this.barPaddingLeft = this.open;
            this.sideNavWidth = this.barPaddingLeft;
        } else {
            this.barPaddingLeft = this.close;
            this.sideNavWidth = this.barPaddingLeft;
        }
        this.status = !this.status;
    }
}
