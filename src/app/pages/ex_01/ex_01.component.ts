import {Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { CrudService } from '../../services';


export class Cliente{
    nome:string;
    cognome: string;
    comune: string;
    sesso: string;
}

@Component({
    selector: 'app-ex-01',
    templateUrl: './ex_01.html',
    styleUrls:['./ex_01.scss']
})
export class Ex01Component implements OnInit {

    cliente: Cliente = new Cliente();
    clienteControl: Cliente = new Cliente();

    constructor(private micheleAjax: CrudService) {

    }

    ngOnInit() {
        this.micheleAjax.getDocument('clienti', 'cliente_1').subscribe(data => {
            this.cliente = data;
            this.clienteControl = Object.assign([], this.cliente);
        });
    }

    update(model: any) {
        this.micheleAjax.updateDocument(this.cliente, 'clienti', 'cliente_1');
    }
}
